--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11
-- Dumped by pg_dump version 15.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: abbtech
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO abbtech;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: abbtech
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: book; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.book (
    bookid integer NOT NULL,
    title character varying(255),
    author character varying(100)
);


ALTER TABLE public.book OWNER TO abbtech;

--
-- Name: book_bookid_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.book_bookid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_bookid_seq OWNER TO abbtech;

--
-- Name: book_bookid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.book_bookid_seq OWNED BY public.book.bookid;


--
-- Name: class; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.class (
    classid integer NOT NULL,
    classname character varying(100)
);


ALTER TABLE public.class OWNER TO abbtech;

--
-- Name: class_classid_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.class_classid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.class_classid_seq OWNER TO abbtech;

--
-- Name: class_classid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.class_classid_seq OWNED BY public.class.classid;


--
-- Name: department; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.department (
    departmentid integer NOT NULL,
    departmentname character varying(100)
);


ALTER TABLE public.department OWNER TO abbtech;

--
-- Name: department_departmentid_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.department_departmentid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.department_departmentid_seq OWNER TO abbtech;

--
-- Name: department_departmentid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.department_departmentid_seq OWNED BY public.department.departmentid;


--
-- Name: professor; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.professor (
    professorid integer NOT NULL,
    firstname character varying(50),
    lastname character varying(50),
    departmentid integer,
    bookid integer
);


ALTER TABLE public.professor OWNER TO abbtech;

--
-- Name: professor_professorid_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.professor_professorid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professor_professorid_seq OWNER TO abbtech;

--
-- Name: professor_professorid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.professor_professorid_seq OWNED BY public.professor.professorid;


--
-- Name: student; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.student (
    studentid integer NOT NULL,
    firstname character varying(50),
    lastname character varying(50),
    cardid integer
);


ALTER TABLE public.student OWNER TO abbtech;

--
-- Name: student_studentid_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.student_studentid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_studentid_seq OWNER TO abbtech;

--
-- Name: student_studentid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.student_studentid_seq OWNED BY public.student.studentid;


--
-- Name: studentclass; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.studentclass (
    studentid integer NOT NULL,
    classid integer NOT NULL
);


ALTER TABLE public.studentclass OWNER TO abbtech;

--
-- Name: studentidcard; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.studentidcard (
    cardid integer NOT NULL,
    cardnumber character varying(20),
    expirydate date
);


ALTER TABLE public.studentidcard OWNER TO abbtech;

--
-- Name: studentidcard_cardid_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.studentidcard_cardid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studentidcard_cardid_seq OWNER TO abbtech;

--
-- Name: studentidcard_cardid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.studentidcard_cardid_seq OWNED BY public.studentidcard.cardid;


--
-- Name: book bookid; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.book ALTER COLUMN bookid SET DEFAULT nextval('public.book_bookid_seq'::regclass);


--
-- Name: class classid; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.class ALTER COLUMN classid SET DEFAULT nextval('public.class_classid_seq'::regclass);


--
-- Name: department departmentid; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.department ALTER COLUMN departmentid SET DEFAULT nextval('public.department_departmentid_seq'::regclass);


--
-- Name: professor professorid; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.professor ALTER COLUMN professorid SET DEFAULT nextval('public.professor_professorid_seq'::regclass);


--
-- Name: student studentid; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.student ALTER COLUMN studentid SET DEFAULT nextval('public.student_studentid_seq'::regclass);


--
-- Name: studentidcard cardid; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.studentidcard ALTER COLUMN cardid SET DEFAULT nextval('public.studentidcard_cardid_seq'::regclass);


--
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.book (bookid, title, author) FROM stdin;
\.


--
-- Data for Name: class; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.class (classid, classname) FROM stdin;
\.


--
-- Data for Name: department; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.department (departmentid, departmentname) FROM stdin;
\.


--
-- Data for Name: professor; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.professor (professorid, firstname, lastname, departmentid, bookid) FROM stdin;
\.


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.student (studentid, firstname, lastname, cardid) FROM stdin;
\.


--
-- Data for Name: studentclass; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.studentclass (studentid, classid) FROM stdin;
\.


--
-- Data for Name: studentidcard; Type: TABLE DATA; Schema: public; Owner: abbtech
--

COPY public.studentidcard (cardid, cardnumber, expirydate) FROM stdin;
\.


--
-- Name: book_bookid_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.book_bookid_seq', 1, false);


--
-- Name: class_classid_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.class_classid_seq', 1, false);


--
-- Name: department_departmentid_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.department_departmentid_seq', 1, false);


--
-- Name: professor_professorid_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.professor_professorid_seq', 1, false);


--
-- Name: student_studentid_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.student_studentid_seq', 1, false);


--
-- Name: studentidcard_cardid_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.studentidcard_cardid_seq', 1, false);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (bookid);


--
-- Name: class class_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.class
    ADD CONSTRAINT class_pkey PRIMARY KEY (classid);


--
-- Name: department department_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.department
    ADD CONSTRAINT department_pkey PRIMARY KEY (departmentid);


--
-- Name: professor professor_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_pkey PRIMARY KEY (professorid);


--
-- Name: student student_cardid_key; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_cardid_key UNIQUE (cardid);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (studentid);


--
-- Name: studentclass studentclass_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.studentclass
    ADD CONSTRAINT studentclass_pkey PRIMARY KEY (studentid, classid);


--
-- Name: studentidcard studentidcard_pkey; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.studentidcard
    ADD CONSTRAINT studentidcard_pkey PRIMARY KEY (cardid);


--
-- Name: professor professor_bookid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_bookid_fkey FOREIGN KEY (bookid) REFERENCES public.book(bookid);


--
-- Name: professor professor_departmentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_departmentid_fkey FOREIGN KEY (departmentid) REFERENCES public.department(departmentid);


--
-- Name: student student_studentidcard_cardid_fk; Type: FK CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_studentidcard_cardid_fk FOREIGN KEY (cardid) REFERENCES public.studentidcard(cardid);


--
-- Name: studentclass studentclass_classid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.studentclass
    ADD CONSTRAINT studentclass_classid_fkey FOREIGN KEY (classid) REFERENCES public.class(classid);


--
-- Name: studentclass studentclass_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.studentclass
    ADD CONSTRAINT studentclass_studentid_fkey FOREIGN KEY (studentid) REFERENCES public.student(studentid);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: abbtech
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

