package org.abbtech.exercise19.repository;

import org.abbtech.exercise19.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> { }
