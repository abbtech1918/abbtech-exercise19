package org.abbtech.exercise19.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "departmentid")
    private Long id;

    @Column(name = "departmentname")
    private String departmentName;


    @OneToMany(mappedBy = "department", cascade = CascadeType.REFRESH)
    private List<Professor> professors;

}
