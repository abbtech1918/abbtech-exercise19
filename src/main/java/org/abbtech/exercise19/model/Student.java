package org.abbtech.exercise19.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "studentid")
    private Long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cardid")
    private StudentCard studentCard;

    @ManyToMany
    @JoinTable(
            name = "studentclass",
            joinColumns = @JoinColumn(name = "studentid",referencedColumnName = "studentid"),
            inverseJoinColumns = @JoinColumn(name = "classid",referencedColumnName = "classid")
    )
    private Set<Class> classes = new HashSet<>();
}
