package org.abbtech.exercise19.model;

import jakarta.persistence.*;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "class")
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "classid")
    private Long id;

    @Column(name = "classname")
    private String className;

    @ManyToMany(mappedBy = "classes")
    private Set<Student> students = new HashSet<>();

}
