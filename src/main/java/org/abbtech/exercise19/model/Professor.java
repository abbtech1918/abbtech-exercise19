package org.abbtech.exercise19.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "professor")
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "professorid")
    private Long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "departmentid", referencedColumnName = "departmentid")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "bookid", referencedColumnName = "bookid")
    private Book book;

}
