package org.abbtech.exercise19.model;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
@Table(name = "studentidcard")
public class StudentCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cardid")
    private Long id;

    @Column(name = "cardnumber")
    private String cardNumber;

    @Column(name = "expirydate")
    private Timestamp expiryDate;

    @OneToOne(mappedBy = "studentCard", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Student student;
}
